<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ScaleApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.selectbox.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <div class="single-page-nav">
        <div class="header navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="navbar ">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <img src='images/logo.png'></a>
                            <button type="button" class='navbar-toggle' data-toggle='collapse' data-target='#responcive-menu'>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-example" id="responcive-menu">
                            <ul class="nav navbar-nav right nav-tabs bs-example-js-navbar-scrollspy" role="tablist">
                                <li>
                                    <a href='#section1' class="page-scroll">Home</a>
                                </li>
                                <li>
                                    <a href='#section2' class="page-scroll">Business</a>
                                </li>
                                <li>
                                    <a href='#section3' class="page-scroll">About</a>
                                </li>
                                <li>
                                    <a href='#section4' class="page-scroll">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="home section" id="section1">
            <div class="home-app">
                <div class="container">
                    <div class="row">
                        <div id="carousel-example-generic" class="carousel slide"  data-interval="false" data-ride="carousel">
                            <ol class="pagination">
                                <li data-target="#carousel-example-generic">
                                    <a href="#carousel-example-generic" role="button" data-slide="prev">&lsaquo;</a>
                                </li>
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="">
                                    <a href="#">1</a>
                                </li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class="">
                                    <a href="#">2</a>
                                </li>
                               <!--  <li data-target="#carousel-example-generic" data-slide-to="2" class="">
                                    <a href="#">3</a>
                                </li>
                                <li data-target="#carousel-example-generic" data-slide-to="3" class="">
                                    <a href="#">4</a>
                                </li> -->
                                <li data-target="#carousel-example-generic">
                                    <a href="#carousel-example-generic" role="button" data-slide="next">&rsaquo;</a>
                                </li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-2 col-sm-offset-2 col-lg-2 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>Camera Zoom Extreme</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-bot">
                                                    <img src="images/camera_zoom.png" >
                                                    <a href="https://itunes.apple.com/app/id661625691?mt=8&at=10l6dK" class="ios-url"></a>
                                                </div>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.camerazoomfree" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id661625691?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.camerazoomfree" class="google_play_icon"></a>
                                            </div>
                                            <div class="col-lg-2 col-sm-2 col-xs-6 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>
                                                        <span>jumpy bird</span>
                                                    </h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/jumpy_bird.png" >
                                                <a href="https://itunes.apple.com/app/id866033363?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.JumpyBird " class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id866033363?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.JumpyBird " class="google_play_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Slow Motion Camera</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/slow_Mmotion.png" >
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8&at=10l6dK" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>iSuperMic</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/iSuperMic.png" >
                                                <a href="https://itunes.apple.com/app/id602725810?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id602725810?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div>
                                           <!--  <div class="col-xs-6 col-sm-2 col-sm-offset-2 col-lg-2 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>i-Ruler</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/i-Ruler.png" >
                                                <a href="https://itunes.apple.com/app/id474785950?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.ruler" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id474785950?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.ruler"  class="google_play_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>17</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/17.png" >
                                                <a href="https://itunes.apple.com/app/id875222440?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id875222440?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Military Binoculars</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/military_binoculars.png" >
                                                <a href="https://itunes.apple.com/app/id641333599?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id641333599?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Burst Camera Extreme</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/burst_camera.png" >
                                                <a href="https://itunes.apple.com/app/id667487789?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id667487789?mt=8&at=10l6dK"  class="app_store_icon"></a>
                                            </div> -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                           <!--  <div class="col-xs-6 col-sm-2 col-sm-offset-2 col-lg-2 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>Camera Zoom Extreme</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-bot">
                                                    <img src="images/camera_zoom.png" >
                                                    <a href="https://itunes.apple.com/app/id661625691?mt=8&at=10l6dK" class="ios-url"></a>
                                                </div>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.camerazoomfree" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id661625691?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.camerazoomfree" class="google_play_icon"></a>
                                            </div>
                                            <div class="col-lg-2 col-sm-2 col-xs-6 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>
                                                        <span>jumpy bird</span>
                                                    </h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/jumpy_bird.png" >
                                                <a href="https://itunes.apple.com/app/id866033363?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.JumpyBird " class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id866033363?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.JumpyBird " class="google_play_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Slow Motion Camera</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/slow_Mmotion.png" >
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8&at=10l6dK" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id816154032?mt=8" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>iSuperMic</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/iSuperMic.png" >
                                                <a href="https://itunes.apple.com/app/id602725810?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id602725810?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div> -->
                                            <div class="col-xs-6 col-sm-2 col-sm-offset-2 col-lg-2 pos all_dev">
                                                <div class="h3_c">
                                                    <h3>i-Ruler</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/i-Ruler.png" >
                                                <a href="https://itunes.apple.com/app/id474785950?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.ruler" class="android-url"></a>
                                                <a href="https://itunes.apple.com/app/id474785950?mt=8&at=10l6dK" class="app_store_icon"></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.IdanS.ruler"  class="google_play_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>17</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/17.png" >
                                                <a href="https://itunes.apple.com/app/id875222440?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id875222440?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Military Binoculars</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/military_binoculars.png" >
                                                <a href="https://itunes.apple.com/app/id641333599?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id641333599?mt=8&at=10l6dK" class="app_store_icon"></a>
                                            </div>
                                            <div class="col-xs-6 col-sm-2 col-lg-2 pos">
                                                <div class="h3_c">
                                                    <h3>Burst Camera Extreme</h3>
                                                </div>
                                                <div class="clearfix"></div>
                                                <img src="images/burst_camera.png" >
                                                <a href="https://itunes.apple.com/app/id667487789?mt=8&at=10l6dK" class="ios-url"></a>
                                                <a href="https://itunes.apple.com/app/id667487789?mt=8&at=10l6dK"  class="app_store_icon"></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                               
                               
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"></a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"></a>
                </div>
            </div>
        </div>
        <div class="philosophy">
            <div class="container">
                <hr>
                <p>
                    <img src="images/quote.png"  class="leftimg">
                    We believe that the best products are the one, which are very easy to use, without learning curve and with great technology to back this up. We invest massive amount of time figuring up users needs and design the simplest native UI to it.
                    <br/>
                    Our apps scale to users needs; we always keep serious attention to users feedback and improve each app according to those. Evolving in time for the users. This is why our products become more relevant and great in time.
                </p>
                <hr></div>
        </div>
    </div>
    <div class="business section" id="section2">
        <div class="container">
            <h2>business</h2>
            <span class='business-top'>Scale your business, Build your app!</span>
            <span class='business-bottom'>
                We could also build your app, if it’s an SDK for your startup or an app for your business,
                <br/>
                please contact us on
                <br/>
                <a href="mailto:biz@scaleapp.net">biz@scaleapp.net</a>
            </span>
        </div>
    </div>
    <div class="about section" id="section3">
        <div class="container">
            <h2>about</h2>
            <hr/>
            <span>
                Founded in 2011, Scaleapp develop large-scale consumers and business applications. Now, with more than 1 million active users a month on Android and iOS.
            </span>
        </div>
    </div>

    <div class="contact-us section" id="section4">
        <div class="container">
            <div class="row">
                <h2>Contact Us</h2>
                <hr>
                <form method="post" accept-charset="UTF-8" action="mailto.php">
                    <div class="yola-form-field">
                        <p class="form-paragraph">
                            Please use this form to inform us about any application issue, features suggestion or anything else on your mind. We’ll do our best to fix any issue and contact you as soon as possible.
                        </p>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 ">
                                <div class="yola-form-field yff_f">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_1">Application Name</label>
                                    </p>
                                    <select id="yola_form_widget_I19_1" name="list_1">
                                        <option value="Binoculars">Binoculars</option>
                                        <option value="i-Hear">i-Hear</option>
                                        <option value="iMagnifier">iMagnifier</option>
                                        <option value="I&#39;m Busy SMS Sender">I'm Busy SMS Sender</option>
                                        <option value="Slow Motion Camera">Slow Motion Camera</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <input type="hidden" name="label_1" value="Application Name"></div>
                                <div class="clearfix"></div>
                                <div class="yola-form-field">
                                    <p class="label t_w">
                                        <label for="yola_form_widget_I19_2">
                                            If other please insert
                                            <br/>
                                            App Name below
                                        </label>
                                    </p>
                                    <input id="yola_form_widget_I19_2" class="text" name="text_1" type="text" value="">
                                    <input type="hidden" name="label_2" value="If other please insert App Name below"></div>
                                <div class="clearfix"></div>
                                <div class="yola-form-field clearfix rating-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_3">Application Satisfaction</label>
                                    </p>
                                    <select id="yola_form_widget_I19_3" name="list_2">
                                        <option value="★★★★★ Great application ">★★★★★ Great application</option>
                                        <option value="★★★★ Good application ">★★★★ Good application</option>
                                        <option value="★★★ Nice application ">★★★ Nice application</option>
                                        <option value="★★ Bad application ">★★ Bad application</option>
                                        <option value="★ It&#39;s a disaster! ">★ It's a disaster!</option>
                                    </select>
                                    <input type="hidden" name="label_3" value="Application Satisfaction"></div>
                                <div class="yola-form-field clearfix">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_4">iOS Version (Optional)</label>
                                    </p>
                                    <input id="yola_form_widget_I19_4" class="text" name="text_2" type="text" value="">
                                    <input type="hidden" name="label_4" value="iOS Version (Optional)"></div>
                                <div class="yola-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_5">Device (Optional)</label>
                                    </p>
                                    <input id="yola_form_widget_I19_5" class="text" name="text_3" type="text" value="">
                                    <input type="hidden" name="label_5" value="Device (Optional)"></div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-left">
                                <div class="yola-form-field">
                                    <input type="hidden" name="label_6" value="About you:">
                                    <p class="form-paragraph form_label">About you:</p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="yola-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_7">Name (Required)</label>
                                    </p>
                                    <input id="yola_form_widget_I19_7" class="text" name="text_4" type="text" value="">
                                    <input type="hidden" name="label_7;" value="Name (Required)"></div>
                                <div class="yola-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_8">Email (Required)</label>
                                    </p>
                                    <input id="yola_form_widget_I19_8" class="text" name="text_5" type="text" value="">
                                    <input type="hidden" name="label_8" value="Email (Required)"></div>
                                <div class="yola-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_9">Phone (Optional)</label>
                                    </p>
                                    <input id="yola_form_widget_I19_9" class="text" name="text_6" type="text" value="">
                                    <input type="hidden" name="label_9" value="Phone (Optional)"></div>
                                <div class="yola-form-field">
                                    <p class="label">
                                        <label for="yola_form_widget_I19_10">Message (Required)</label>
                                    </p>
                                    <textarea id="yola_form_widget_I19_10" name="textarea_7"></textarea>
                                    <input type="hidden" name="label_10;" value="Message (Required)"></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="center">
                        <div class="captcha">
                            <div id="recaptcha_widget" style="display:none">
                                <script type="text/javascript">
                                            var RecaptchaOptions = {
                                                theme: 'custom',
                                                custom_theme_widget: 'recaptcha_widget'
                                            };
                                        </script>
                                <div class="control-group">
                                    <!--<label class="control-label">reCAPTCHA</label>
                                -->
                                <div class="controls">
                                    <a id="recaptcha_image" href="#" class="thumbnail"></a>
                                    <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>
                                </div>
                            </div>

                            <div class="control-group">

                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" class="input-recaptcha" />
                                        <a class="btn" href="javascript:Recaptcha.reload()">
                                            <span class="glyphicon glyphicon-refresh"></span>
                                        </a>
                                        <a class="btn recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')">
                                            <span class="glyphicon glyphicon-volume-up"></span>
                                        </a>
                                        <!--<a class="btn recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')">
                                        <span class="glyphicon glyphicon-exclamation-sign"></span>
                                    </a>
                                    -->
                                    <a class="btn" href="javascript:Recaptcha.showhelp()">
                                        <span class="glyphicon glyphicon-exclamation-sign"></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <script type="text/javascript"
                                        src="<?php echo $recaptcha_url; ?>"></script>

                <noscript>
                    <iframe src="<?php echo $recaptcha_noscript_url; ?>
                        "
                                        height="300" width="500" frameborder="0">
                    </iframe>
                    <br>
                    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                    <input type="hidden" name="recaptcha_response_field"
                                       value="manual_challenge"></noscript>

                <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LcrK9cSAAAAALEcjG9gTRPbeA0yAVsKd8sBpFpR"></script>

                <noscript>
                    <iframe src="<?php echo $recaptcha_noscript_url; ?>
                        "
                                        height="300" width="500" frameborder="0">
                    </iframe>
                    <br>
                    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                    <input type="hidden" name="recaptcha_response_field" value="manual_challenge"></noscript>

                <input class="submit" type="submit" value="Submit"></div>
        </form>
    </div>
</div>
</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script type = "text/javascript" src = "js/jquery.selectbox-0.2.js" ></script>
<script src="js/device.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>

<script>
                                            $(document).ready(function () {
                                                $('body').scrollspy({target: '.navbar-example'});
                                                $("#yola_form_widget_I19_1").selectbox();
                                                $("#yola_form_widget_I19_3").selectbox();
                                            });
                                            $(function () {
                                                $('a.page-scroll').bind('click', function (event) {
                                                    console.log("sds");
                                                    var $anchor = $(this);
                                                    $('html, body').stop().animate({
                                                        scrollTop: $($anchor.attr('href')).offset().top
                                                    }, 1500, 'easeInOutExpo');
                                                    $('.collapse').removeClass('in');
                                                    event.preventDefault();
                                                });
                                                $('.carousel').carousel({
                                                    interval: 3000
                                                })
                                            });

        </script>
</body>
</html>